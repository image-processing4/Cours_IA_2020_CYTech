function [image] = filtre_moyenne(image, r)
image=add_rows(add_cols(image,r),r);
[x,y]=size(image);

size_mask_x = 2*r+1;
size_mask_y = 2*r+1;

for i=r+1:x-r
    for j=r+1:y-r
        start_point_x=uint16(i-double(size_mask_x/2)+0.5);
        stop_point_x=uint16(i+double(size_mask_x/2)-0.5);
        start_point_y=uint16(j-double(size_mask_y/2)+0.5);
        stop_point_y=uint16(j+double(size_mask_y/2)-0.5);
        submatrix=image(start_point_x:stop_point_x,start_point_y:stop_point_y);
        value = mean(submatrix(:));
        image(i,j)=uint16(value);
    end
    %imshow(image)
end
end