function [image_etiree] = etirement(image)
[x,y]=size(image);
max_value = max(max(image)) ;% min(image(:))
min_value = min(min(image));
image_etiree = double(image-min_value).*double(255/(max_value-min_value)); % avec matlab pas besoin de faire des doubles boucles
image_etiree = uint8(image_etiree);
end

