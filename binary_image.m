function [cube] = binary_image(size_black_square, size_white_square, start_white)
cube = zeros(size_black_square)
stop_white = start_white + size_white_square -1
cube(start_white:stop_white, start_white:stop_white) = ones(size_white_square)*255
end

