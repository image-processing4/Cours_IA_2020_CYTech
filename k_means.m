function [image] = k_means(image,k)

% choisir de mani�re al�atoire les moyennes des k classes 
k_means = zeros(1,k);
old_k_means = ones(1,k);

for i=1:k
    k_means(1,i) = floor(rand*255);
end
k_means = sort(k_means); 

% calcul de l'histogramme
histogramme = histogram(image); 
histogramme_classe = [histogramme;zeros(1,256)]; 


% convergence des classes 
while not(isequal(old_k_means, k_means))
    classe = zeros(1,k); 
    
    % on range les pixels en fonction des moyennes
    for i=1:256
        value = histogramme(1,i);
        distance = zeros(1,k);
        for j=1:k
            distance(1,j) = abs(value-k_means(1,j));
        end
        [M,index] = min(distance); 
        classe(1,index) = classe(1,index) + (histogramme(1,i)*histogramme(2,i));
        histogramme_classe(3,i)=index; 
    end

    old_k_means = k_means
    
    % on recalcule les moyennes 
    for i=1:k
        somme = 0; 
        for j=1:256 
            if histogramme_classe(3,j) == i
                somme = somme+histogramme_classe(2,j);
            end
        end
        k_means(1,i) = floor(classe(1,i)/somme);
    end
end

% changement des valeurs des pixels
[x,y]=size(image);
for i=1:x
    for j=1:y
        value = image(i,j);
        classe_pixel = histogramme_classe(3,value+1);
        image(i,j) = k_means(1,classe_pixel);
    end
end
       
end

