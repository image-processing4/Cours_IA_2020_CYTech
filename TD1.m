% TD1-Creation and Representation Images
% AuroreWilbrink

% Exercice 1
i = binary_image(200,100,100)
imshow(i)

% Exercice 2 et 4
% Cr�er un damier 
imshow(checkerboard(10),'InitialMagnification','fit')

% Exercice 3 
% image en niveau de gris (4*7)
imshow(niveau_gris(),'InitialMagnification','fit')

% Exercice 5 
size=100
image=zeros(size)
for i=1:size
    k=0
    for j=5:size
        k=k+1/size
        image(i,j)=k
    end
end
imshow(image,'InitialMagnification','fit')

% Exercice 6 : 
matrice=exo_six(3)
R=[0.5,0,1 ; 
    0,1,0 ; 
    1,0,0.5];
G=[0.5,0.6,0 ; 
   0.6,0,0.6 ; 
    0,0.6,0.5]
B=[0.5,1,0 ; 
    1,0,1 ; 
    0,1,0.5]
I=cat(3,R,G,B)
% imshow(I,'InitialMagnification','fit')

R=[0.5, 0, 1, 
    0, 1, 0,
    1, 0, 0.5]
G=[0.5, 1, 0, 
    1, 0, 1, 
    0, 1, 0.5]
B=[0.5, 0, 0, 
    0, 0, 0, 
    0, 0, 0.5]
J=cat(3,R,G,B)

% exercice 7
map=[0.5, 0.5, 0.5; 0, 1, 0; 1, 0, 0] %gris, vert, rouge
map =[0.2 0.2 0.2; 0 1 0; 1 0 0] % noir, vert, rouge
K=[1 2 3; 2 3 2; 3 2 1]
imshow(K,map, 'InitialMagnification','fit')






