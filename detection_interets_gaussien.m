function [result] = detection_interets_gaussien(image, lambda, seuil)
mask= (1/16) * [1 2 1;2 4 2;1 2 1]
result = detection_interets(apply_mask(mask, mask,image), 0.1, 1)
end

