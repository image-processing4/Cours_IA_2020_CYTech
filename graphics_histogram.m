function [] = graphics_histogram(histogram)
X=uint8(histogram(1,:))
Y=uint8(histogram(2,:))
plot(X,Y)
end

