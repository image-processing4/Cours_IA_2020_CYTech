clear
image= imread('pout.jpg');

S = uint8(rand*255); % Choisir S de mani�re al�atoire
S_old = -1;
histogramme = histogram(image); % histogramme de l'image

while not(S_old == S) % tnat que S n'est pas constant, on recalcule S
    S = calcul_S(image, histogramme, S);
    S_old = S;
end
figure(1);
imshow(apply_seuil_binaire(image, S),'InitialMagnification','fit'); title('option 1');
figure(2);
imshow(apply_seuil_S(image, histogramme, S),'InitialMagnification','fit'); title('option 2'); 

image = imread('pepper.bmp');
image = image(:,:,1);

S = uint8(rand*255); % Choisir S de mani�re al�atoire
S_old = -1;
histogramme = histogram(image); % histogramme de l'image

while not(S_old == S) % tnat que S n'est pas constant, on recalcule S
    S = calcul_S(image, histogramme, S);
    S_old = S;
end
figure(1);
imshow(apply_seuil_binaire(image, S),'InitialMagnification','fit'); title('option 1');
figure(2);
imshow(apply_seuil_S(image, histogramme, S),'InitialMagnification','fit'); title('option 2'); 

% utilisation de k_means 
subplot(2,2,1);
imshow(k_means(image,2)); title("k=2");
subplot(2,2,2);
imshow(k_means(image,4)); title("k=4");
subplot(2,2,3);
imshow(k_means(image,8)); title("k=8");
subplot(2,2,4);
imshow(image); title("originale");







    
  


