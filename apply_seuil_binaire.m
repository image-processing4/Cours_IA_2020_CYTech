function [image] = apply_seuil_binaire(image, seuil)
image = (image<seuil);
end

