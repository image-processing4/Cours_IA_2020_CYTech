function [result] = detection_interets(image, lambda, seuil)

image = add_cols(add_rows(image,1),1); % add margins

mask_x=[-1 0 1 ; -1 0 1 ; -1 0 1];
mask_y=[-1 -1 -1 ; 0 0 0 ; 1 1 1];

[x,y]=size(image); % size of the image with margins 

for i=2:x-1 % 1st row and last row are margins 
    for j=2:y-1
        Ix(i,j) = abs(sum(sum(double(mask_x) .* double(image(i-1:i+1, j-1:j+1)))));
        Iy(i,j) = abs(sum(sum(double(mask_y) .* double(image(i-1:i+1,j-1:j+1)))));
    end
end

Ix2=Ix.^2;
Iy2=Iy.^2;

c = Ix2 .* Iy2 - Ix.*Iy - lambda*((Ix2+Iy2).^2);
result = (c>=seuil); % 1 or 0

end

