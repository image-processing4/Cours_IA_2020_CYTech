function [image] = filtre_median(image)

image=add_rows(add_cols(image,1),1)
[x,y]=size(image)

for i=2:x-1
    for j=2:y-1
        points = [image(i-1,j-1), image(i-1,j), image(i-1,j+1), image(i,j+1), image(i+1,j), image(i,j), image(i+1,j+1), image(i+1,j), image(i+1,j-1)];
        image(i,j)=median(points);
    end
end
end

