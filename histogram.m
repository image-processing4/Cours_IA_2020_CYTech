function [histogram] = histogram(image)

[x,y] = size(image);
histogram = zeros(2,256);% vecteur 256 colonnes 2 lignes 
histogram(1,1:256)=0:255;

for i=1:x
    for j=1:y
        histogram(2,image(i,j)+1)=histogram(2,image(i,j)+1)+1;
    end
end
end

