image = imread('lena.jpg')
%image = binary_image(100,50,25)
imshow(image)

% Exercice 1 : 
imshow(prewitt_simple(image)); title('Prewitt Image _ without threshold');
imshow(prewitt_threshold(image,100)); title('Prewitt Image _ with threshold');

imshow(sobel_simple(image)); title('Sobel Image _ without threshold');
imshow(sobel_threshold(image,100)); title('Sobel Image _ with threshold');


% Questions avanc�es A.1)
imshow(prewitt_threshold(image,50)); title('Prewitt Image _ with threshold');
imshow(prewitt_threshold(image,150)); title('Prewitt Image _ with threshold');
imshow(prewitt_threshold(image,225)); title('Prewitt Image _ with threshold');

imshow(sobel_threshold(image,50)); title('Sobel Image _ with threshold');
imshow(sobel_threshold(image,150)); title('Sobel Image _ with threshold');
imshow(sobel_threshold(image,225)); title('Sobel Image _ with threshold');

% Exercice 2 :
image = imread('circuit.tif')
figure(4)
imshow(image)
mask1=[-1 -1 -1;-1 9 -1; -1 -1 -1]
mask2 = [-1 -1 -1;-1 8 -1; -1 -1 -1]
mask3 = 1/9*ones(3)

figure(1)
imshow(apply_mask(mask1, mask1, image)); title('mask 1 ');
figure(2)
imshow(apply_mask(mask2, mask2, image)); title('mask 2 ');
figure(3)
imshow(apply_mask(mask3, mask3, image)); title('mask 3 ');

% TD 6 : appliquer Canny
mask_canny = [-1 -1 -1;-1 8 -1; -1 -1 -1]
figure(1)
imshow(apply_mask(mask_canny, mask_canny, image)); title('Canny');








