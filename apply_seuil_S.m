function [image] = apply_seuil_S(image, histogramme, seuil)

S1 = 0 ;
S2 = 0;

for i = 1 : 256
    if (histogramme(1,i) < seuil)
        S1 = S1 + (histogramme(2,i)/(seuil));
    else
        S2 = S2 + (histogramme(2,i)/(255-seuil+1));
    end
end

[x,y] = size(image);
for i=1:x
    for j=1:y
        pixel = image(i,j);
        if pixel < seuil
            image(i,j) = S1;
        else 
            image(i,j) = S2;
        end
    end
end 
end
