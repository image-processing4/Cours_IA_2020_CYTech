function [histo_cumule] = histogramme_cumule(histogram)
histo_cumule=zeros(2,256)
histo_cumule(1,1:256)=0:255
histo_cumule(2,1)=histo(2,1)
for i=2:256
    histo_cumule(2,i)=histo(2,i)+histo_cumule(2,i-1)
end
end

