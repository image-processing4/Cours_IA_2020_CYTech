function [new_image] = apply_mask(mask_x, mask_y,image)

[x,y]=size(image); % size of the image with margins 
new_image = zeros(x,y);

for i=2:x-1
    for j=2:y-1
        Gx = sum(sum(double(mask_x) .* double(image(i-1:i+1, j-1:j+1))));
        Gy = sum(sum(double(mask_y) .* double(image(i-1:i+1,j-1:j+1))));
        new_image(i,j) = sqrt(Gx.^2 + Gy.^2);
    end
end
new_image =uint8(new_image);

end

