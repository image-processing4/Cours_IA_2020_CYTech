function [new_image] = prewitt_simple(image)
image = add_cols(add_rows(image,1),1); % add margins
mask_prewitt_x=[-1 0 1; -1 0 1; -1 0 1];
mask_prewitt_y=[-1 -1 -1; 0 0 0 ; 1 1 1];
new_image = apply_mask(mask_prewitt_x, mask_prewitt_y, image);
end

