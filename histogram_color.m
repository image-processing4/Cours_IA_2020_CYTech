function [] = histogram_color(image)
R=double(reshape(image(:,:,1).',1,[])); % .' to transpose the matrix into a 1 by all the columns (1,[])
G=double(reshape(image(:,:,2).',1,[]));
B=double(reshape(image(:,:,3).',1,[]));
C=[R;G;B].' / 255;
scatter3(R, G, B,[],C,'.');

end

