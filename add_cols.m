function [image] = add_cols(image, nb_cols)
[x,y]=size(image)
black_col=zeros(x,1)
for i=1:nb_cols
    image = [black_col image black_col]
end

end

