function [image] = add_rows(image,nb_rows)
[x,y]=size(image)
black_row=zeros(1,y)
for i=1:nb_rows
    image =[black_row; image; black_row]
end
end

