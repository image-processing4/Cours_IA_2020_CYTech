function [new_image] = prewitt_threshold(image,seuil)
new_image = prewitt_simple(image);
new_image = max(new_image, seuil);
new_image(new_image == round(seuil)) =0; 
new_image=im2bw(new_image);
end
