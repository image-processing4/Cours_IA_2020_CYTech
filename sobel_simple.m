function [new_image] = sobel_simple(image)
image = add_cols(add_rows(image,1),1); % add margins

mask_sobel_x=[1 0 -1;2 0 -2; 1 0 -1];
mask_sobel_y=[1 2 1 ; 0 0 0 ; -1 -2 -1];

new_image = apply_mask(mask_sobel_x, mask_sobel_y, image);
end 
