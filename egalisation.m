function [image_egalisee] = egalisation(image)

histo=histogram(image)

[x,y] = size(image)
histo_cumule = zeros(2,256)% vecteur 256 colonnes 2 lignes 
histo_cumule(1,1:256)=0:1:255

histo_cumule(2,1)=histo(2,1)
for i=2:256
    histo(2,i)
    histo_cumule(2,i-1)
    histo_cumule(2,i)=histo(2,i)+histo_cumule(2,i-1)
end
nb_pixel = numel(image)

for i=1:x
    for j=1:y
        image_egalisee(i,j)=histo_cumule(2,image(i,j)+1)*double(255/(nb_pixel));
    end
end

image_egalisee=uint8(image_egalisee)

end

