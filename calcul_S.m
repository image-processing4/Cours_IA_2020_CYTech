function [S] = calcul_S(image, histogramme, seuil)

S1 = 0 ;
S2 = 0;
for i = 1 : 256
    if (histogramme(1,i) < seuil)
        S1 = S1 + (histogramme(2,i)/(seuil));
    else
        S2 = S2 + (histogramme(2,i)/(255-seuil+1));
    end
end
S = (S1+S2)/2;
end

