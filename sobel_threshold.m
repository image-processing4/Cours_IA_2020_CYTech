function [new_image] = sobel_threshold(image, seuil)
new_image = sobel_simple(image);
new_image = max(new_image, seuil);
new_image(new_image == round(seuil)) = 0; 
new_image=im2bw(new_image); 
end

