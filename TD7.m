% TD 7 : d�tection des points d'int�r�ts 


% d�tection sur image originale 
clear 
image = binary_image(100,50,5);
imshow(image,'InitialMagnification','fit');
imshow(detection_interets(image,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 
image = prewitt_simple(image)
imshow(image,'InitialMagnification','fit');
imshow(detection_interets(image,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 


% m�me chose avec circuit
clear 
image= imread('circuit.tif')
imshow(image,'InitialMagnification','fit');
imshow(detection_interets(image,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 
image_contours = prewitt_simple(image)
imshow(image_contours,'InitialMagnification','fit');
imshow(detection_interets(image_contours,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 

% variation des seuils

figure(1)
imshow(detection_interets(image,0.1,1),'InitialMagnification','fit'); title('seuil = 1')% image, lambda, seuil 
figure(2)
imshow(detection_interets(image,0.1,500),'InitialMagnification','fit'); title('seuil = 500') % image, lambda, seuil 
figure(3)
imshow(detection_interets(image,0.1,3000),'InitialMagnification','fit'); title('seuil = 3000')% image, lambda, seuil 
figure(4)
imshow(detection_interets(image,0.1,10000),'InitialMagnification','fit'); title('seuil = 10000')% image, lambda, seuil 


% variation de lambda 
figure(1)
imshow(detection_interets(image,0.1,10000),'InitialMagnification','fit'); title('seuil = 10000, lambda = 0.1')% image, lambda, seuil 
figure(2)
imshow(detection_interets(image,0.01,10000),'InitialMagnification','fit'); title('seuil = 10000, lambda = 0.01')% image, lambda, seuil 
figure(3)
imshow(detection_interets(image,1,10000),'InitialMagnification','fit'); title('seuil = 10000, lambda = 0')% image, lambda, seuil 
figure(4)
imshow(detection_interets(image,0.001,10000),'InitialMagnification','fit'); title('seuil = 10000, lambda = 0.001')% image, lambda, seuil 

% circuit avec gaussien 
clear 
image= imread('circuit.tif')
figure(1)
imshow(image,'InitialMagnification','fit');
figure(2)
imshow(detection_interets_gaussien(image,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 
image_contours = prewitt_simple(image)
figure(3)
imshow(image_contours,'InitialMagnification','fit');
imshow(detection_interets_gaussien(image_contours,0.1,1),'InitialMagnification','fit'); % image, lambda, seuil 
